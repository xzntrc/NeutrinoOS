#include <stdint.h>
#include <stddef.h>
#include <limine.h>
#include <libkern/log.h>
#include <libkern/releaseinfo.h>

__attribute__((noreturn)) void _start(void) {
  printk_boot("%s\n", RELEASE);
  printk_boot("%s\n", COPYRIGHT);
  printk_boot("Kernel compiled on " __DATE__ " at " __TIME__ "\n\n");
  printk_boot("Coming soon..\n");

  while (1) {
    __asm__ __volatile__("hlt");
  }
}
