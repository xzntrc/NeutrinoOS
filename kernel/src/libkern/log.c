#include <libkern/log.h>
#include <libkern/string.h>
#include <stdint.h>
#include <limine.h>

static volatile struct limine_terminal_request term_req = {
  .id = LIMINE_TERMINAL_REQUEST,
  .revision = 0
};


static void _puts(const char* str) {
  struct limine_terminal* terminal = term_req.response->terminals[0];
  term_req.response->write(terminal, str, kstrlen(str));
}


void printk_boot(const char* fmt, ...) {
  va_list ap;
  va_start(ap, fmt);

  for (const char* ptr = fmt; *ptr; ++ptr) {
    if (*ptr == '%') {
      ++ptr;

      switch (*ptr) {
        case 'd':
          _puts(dec2str(va_arg(ap, uint64_t)));
          break;
        case 's':
          _puts(va_arg(ap, const char*));
          break;
        case 'x':
          _puts(hex2str(va_arg(ap, uint64_t)));
          break;
        case 'c':
          _puts((char[2]){*ptr, 0});
          break;
      }
    } else {
      _puts((char[2]){*ptr, 0});
    }
  }
}
