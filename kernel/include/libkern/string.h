#ifndef STRING_H_
#define STRING_H_

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>

size_t kstrlen(const char* str);
bool kmemcmp(const char* str1, const char* str2, size_t n);
char* dec2str(int dec);
uint8_t* kstrncpy(uint8_t* dst, const uint8_t* src, const uint8_t len);
void kmemcpy(uint8_t* dst, const uint8_t* src, size_t len);
uint8_t* hex2str(uint32_t hex_num);
void kmemzero(void* ptr, size_t n);
void kmemset(void* ptr, uint64_t data, size_t n);
uint8_t kstrcmp(const char* str1, const char* str2);
uint8_t kstrncmp(const char* str1, const char* str2, size_t n);
uint32_t hex2int(char* hex, size_t len);


#endif // STRING_H_
