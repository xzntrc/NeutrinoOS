# NeutrinoOS

## Pre-built Images
Go to [the most recent job](https://github.com/Ian-Marco-Moffett/NeutrinoOS/actions) and Neutrino.iso will be located under Artifacts.


## Building
Install the following:

- xorriso
- qemu
- gcc
- nasm

Now do:
``make setup; make``
